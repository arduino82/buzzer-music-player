#include "pitches.h"

const int BUZZER_PIN = 6;
const int A_PIN = 13;
const int B_PIN = 12;
const int C_PIN = 8;
const int D_PIN = 7;
const int E_PIN_RED = 11;
const int E_PIN_GREEN = 10;
const int E_PIN_BLUE = 9;
const int F_PIN_RED = 3;
const int F_PIN_GREEN = 5;
const int F_PIN_BLUE = 2;
const int G_PIN = 4;

// Notes
const int NOTES[] = {
  NOTE_FS4, NOTE_F4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_E4, NOTE_D4, NOTE_CS4, NOTE_D4, NOTE_E4,
  NOTE_FS4, NOTE_FS4, NOTE_B4, NOTE_B3, NOTE_CS4, NOTE_D4, NOTE_E4, NOTE_D4, NOTE_CS4, NOTE_A4, NOTE_G4,
  NOTE_FS4, NOTE_F4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_E4, NOTE_D4, NOTE_CS4, NOTE_D4, NOTE_E4,
  NOTE_FS5, NOTE_FS5, NOTE_B5, NOTE_B5, NOTE_CS6, NOTE_D6, NOTE_G5, NOTE_FS5, NOTE_F5, NOTE_D6, NOTE_AS5, NOTE_B5,
};

// Note durations
int NOTE_DURATIONS[] = {
  500, 500, 125, 125, 250, 250, 125, 250, 250, 125,
  500, 125, 500, 125, 125, 250, 250, 125, 250, 250, 125,
  500, 500, 125, 125, 250, 250, 125, 250, 250, 125,
  500, 125, 500, 125, 125, 250, 250, 125, 250, 250, 125, 500,
};

// Pins to activate
const int PINS_TO_ACTIVATE[sizeof(NOTES) / sizeof(NOTES[0])][3] = {
  {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {D_PIN}, {E_PIN_RED, E_PIN_GREEN, E_PIN_BLUE}, {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {E_PIN_RED, E_PIN_GREEN, E_PIN_BLUE}, {D_PIN}, {C_PIN}, {D_PIN}, {E_PIN_RED, E_PIN_GREEN, E_PIN_BLUE},
  {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {B_PIN}, {B_PIN}, {C_PIN}, {D_PIN}, {E_PIN_RED, E_PIN_GREEN, E_PIN_BLUE}, {D_PIN}, {C_PIN}, {A_PIN}, {G_PIN},
  {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {D_PIN}, {E_PIN_RED, E_PIN_GREEN, E_PIN_BLUE}, {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {E_PIN_RED, E_PIN_GREEN, E_PIN_BLUE}, {D_PIN}, {C_PIN}, {D_PIN}, {E_PIN_RED, E_PIN_GREEN, E_PIN_BLUE},
  {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {B_PIN}, {B_PIN}, {C_PIN}, {D_PIN}, {G_PIN}, {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {F_PIN_RED, F_PIN_GREEN, F_PIN_BLUE}, {D_PIN}, {A_PIN}, {B_PIN},
};

const int PIN_ACTIVATION_VALUES[sizeof(NOTES) / sizeof(NOTES[0])][3] = {
  {255, 0, HIGH}, {255, 0, HIGH}, {HIGH}, {0, 75, 25}, {255, 0, HIGH}, {0, 75, 25}, {HIGH}, {HIGH}, {HIGH}, {0, 75, 25},
  {255, 0, HIGH}, {255, 0, HIGH}, {HIGH}, {HIGH}, {HIGH}, {HIGH}, {0, 75, 25}, {HIGH}, {HIGH}, {HIGH}, {HIGH},
  {255, 0, HIGH}, {255, 0, HIGH}, {HIGH}, {0, 75, 25}, {255, 0, HIGH}, {0, 75, 25}, {HIGH}, {HIGH}, {HIGH}, {0, 75, 25},
  {255, 0, HIGH}, {255, 0, HIGH}, {HIGH}, {HIGH}, {HIGH}, {HIGH}, {HIGH}, {255, 0, HIGH}, {255, 0, HIGH}, {HIGH}, {HIGH}, {HIGH},
}; 

void activateNoteLeds(int *ledPins, int *values, int numberOfLeds, unsigned long duration) {
  for (int i = 0; i < numberOfLeds; i++) {
    const bool isAnalog = values[i] != HIGH;
    if (isAnalog) {
      analogWrite(ledPins[i], values[i]);
    } else {
      digitalWrite(ledPins[i], HIGH);
    }
  }
  
  delay(duration);
  
  for (int i = 0; i < numberOfLeds; i++) {
    digitalWrite(ledPins[i], LOW);
  }
}

void setup() {
  pinMode(A_PIN, OUTPUT);
  pinMode(B_PIN, OUTPUT);
  pinMode(C_PIN, OUTPUT);
  pinMode(D_PIN, OUTPUT);
  pinMode(E_PIN_RED, OUTPUT);
  pinMode(E_PIN_GREEN, OUTPUT);
  pinMode(E_PIN_BLUE, OUTPUT);
  pinMode(F_PIN_RED, OUTPUT);
  pinMode(F_PIN_GREEN, OUTPUT);
  pinMode(F_PIN_BLUE, OUTPUT);
  pinMode(G_PIN, OUTPUT);


  for (int currentNote = 0; currentNote < (sizeof(NOTES) / sizeof(NOTES[0])); currentNote++) {
    const int numberOfPinsToActivate = sizeof(PINS_TO_ACTIVATE[0]) / sizeof(PINS_TO_ACTIVATE[0][0]);
    const int pauseBetweenNotes = NOTE_DURATIONS[currentNote] * 0.30;

    tone(BUZZER_PIN, NOTES[currentNote], NOTE_DURATIONS[currentNote]);
    activateNoteLeds(&PINS_TO_ACTIVATE[currentNote][0], &PIN_ACTIVATION_VALUES[currentNote][0], numberOfPinsToActivate, NOTE_DURATIONS[currentNote]);
    delay(pauseBetweenNotes);
    noTone(BUZZER_PIN);
  }
}

void loop() {}
