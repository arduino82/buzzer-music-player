# Buzzer Music Player



## Component List


| Component | Quantity |
| ------ | ------ |
| Arduino Uno R3 | 1 |
| 5mm Red LED | 1 |
| 5mm Blue LED | 1 |
| 5mm Green LED | 1 |
| 5mm Yellow LED | 1 |
| 5mm White LED | 1 |
| LED RGB | 2 |
| 1 kΩ Resistor | 11 |
| Piezo passive buzzer | 1 |


## Circuit Schematic
[Can be found here](https://gitlab.com/arduino82/buzzer-music-player/-/blob/d7f41ade5bea8c325fd7e15ebf74567982126f8e/Circuit_Schematic.png)


## Code
[Can be found here](https://gitlab.com/arduino82/buzzer-music-player/-/blob/d7f41ade5bea8c325fd7e15ebf74567982126f8e/buzzer-music-player.ino)
